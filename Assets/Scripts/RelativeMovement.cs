﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]//проверка наличия компонента

public class RelativeMovement : MonoBehaviour
{
    [SerializeField] private Transform target;

    public float rotationSpeed = 15.0f;
    public float moveSpeed = 6.0f;
    public float jumpSpeed = 15.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float minFall = -1.5f;//небольшая сила внизу нужна для перемещения по пересеченной местности

    public float pushForce = 3.0f;

    private CharacterController _charController;
    private float _vertSpeed;
    private Animator _animator;

    private ControllerColliderHit _contact;//для хранения данных о столкновении между функциями

    // Start is called before the first frame update
    void Start()
    {
        _vertSpeed = minFall;
        _charController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero;//начинаем с вектор 0 0 0 постепенно добавляя компоненты движения

        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");

        if(horInput !=0 || vertInput != 0)//только при нажатии клавиш со стрелками
        {
            movement.x = horInput * moveSpeed; //переписываем строки чтобы добавить скорость движения
            movement.z = vertInput * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);//движение по диагонали также как и вдоль одной оси

            Quaternion tmp = target.rotation;//сохраняем начальную ориентацию. чтобы вернуться  кней после
            target.eulerAngles = new Vector3(0, target.eulerAngles.y, 0);

            movement = target.TransformDirection(movement);//преобразуем направление движения из локальных координат в глобальные
            target.rotation = tmp;

            /*это был грубый вариант*/
            //transform.rotation = Quaternion.LookRotation(movement);//вычисяет кватрнион смотрящий в этом направлении


            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotationSpeed * Time.deltaTime);

        }

        _animator.SetFloat("Speed", movement.sqrMagnitude);

        bool hitGround = false;
        RaycastHit hit;

        if(_vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit))//рассчитываем есть ли под половиной луча пол или объект
        {
            float check = (_charController.height + _charController.radius) / 1.9f;
            hitGround = hit.distance <= check;
        }

        if (hitGround)//соприкасаемся ли с поверхностью? встроенное свойство чар контроллер
        {
            if (Input.GetButtonDown("Jump"))//реакция на конпку при нахождении на поверхности
            {
                _vertSpeed = jumpSpeed;
                
            }
            else
            {
                _vertSpeed = minFall;
                _animator.SetBool("Jumping", false);
            }

        }
        else//если персонаж не стоит применяем гравитацию пока не будет достигнута предельная скорость
        {
            _vertSpeed += gravity * 5 * Time.deltaTime;
            if (_vertSpeed < terminalVelocity)
            {
                _vertSpeed = terminalVelocity;
            }

            if(_contact != null)
            {
                _animator.SetBool("Jumping", true);
            }

            if (_charController.isGrounded)
            {
                if(Vector3.Dot(movement, _contact.normal) < 0)//скалярное произведение двух векторов чтобы понять куда смотрит персонаж. от -1 до +1. чтобы знать какую силу приложить
                {
                    movement = _contact.normal * moveSpeed;
                } else
                {
                    movement += _contact.normal * moveSpeed;
                }
            }
        }

        movement.y = _vertSpeed;

        movement *= Time.deltaTime;
        _charController.Move(movement);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)//получаем инфу о нормали, о столкновении
    {
        _contact = hit;

        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)//проверяем есть ли риджилбади у объекта. если есть прибавляем силу
        {
            body.velocity = hit.moveDirection * pushForce;
        }
    }
}
