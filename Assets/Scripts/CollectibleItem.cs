﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleItem : MonoBehaviour
{


    [SerializeField] private string itemName;

    private void OnTriggerEnter(Collider other)
    {

        Managers.Inventory.AddItem(itemName);
        //Debug.Log("Item collected: " + itemName);
        Destroy(this.gameObject);//именно гейм обжект. без него будет ссылка только на скрипт
    }
}
