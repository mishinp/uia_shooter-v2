﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOperator : MonoBehaviour
{

    public float radius = 1.5f;//расстояние на котором становится возможным активация устройств
    
   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);//возвращает список ближайших объектов

            foreach (Collider hitCollider in hitColliders)
            {

                Vector3 direction = hitCollider.transform.position - transform.position;

                if (Vector3.Dot(transform.forward, direction) > .5f)//скалярное произведение векторов чтобы узнать направление
                {
                    hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);//пытается вызвать функцию независимо от типа объекта
                }
            }
        }
    }
}
