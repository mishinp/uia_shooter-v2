﻿public enum ManagerStatus
{
    Shutdown,
    Initializing,
    Started
}

//перечислены возможные состояние деспетчеров