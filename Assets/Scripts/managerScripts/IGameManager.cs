﻿public interface IGameManager
{
    ManagerStatus status { get; }

    void Startup();
}

//задает структура для других классов. объявляет одно свойство и один метод